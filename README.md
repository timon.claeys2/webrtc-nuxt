# How to use


## Setup

Make sure to install the dependencies:

```bash
pnpm install
```

## Development Server

Start the development server on `http://localhost:3000`:

```bash
pnpm run dev
```

## Build to static files


```bash
# pnpm
pnpm run generate
```

## live site

https://webrtc-nuxt.onrender.com/
