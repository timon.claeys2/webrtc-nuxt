export { Message };

declare global {
    interface Message { message: string, sentByYou: boolean }
}